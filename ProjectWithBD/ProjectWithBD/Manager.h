#ifndef _MANAGER_HPP_
#define _MANAGER_HPP_

#define DBINITCONSTANTS
#define INITGUID

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <oledb.h>
#include <oledberr.h>
#include <msdaguid.h>
#include <msdasql.h>
#include <stdlib.h>
#include <cguid.h>  

class Student;

using std::cout;

class Manager
{
	static Manager * ms_manager;

	Manager();

	~Manager();

	Manager(const Manager &);
	Manager& operator = (const Manager &);

	static void DestroyManager();

	IDBInitialize * pIDBInitialize;
	IRowset * pIRowset;
	DBPROP * pDBPROP;
	IMalloc* pIMalloc;
	IDBCreateCommand * pIDBCreateCommand;
	Student*			newStudent;
	DBCOLUMNINFO* pColumnsInfo;

	std::vector <Student *> students;
	
	struct ComInit
	{
		ComInit()
		{
			::CoInitialize(NULL);
		}

		~ComInit()
		{
			::CoUninitialize();
		}
	} com_init;

	void initParamOfInit();
	void getRows();
	void clearTable();
	bool Manager::insertRow(int id, const char * firstName, const char * lastName, int age, int averageScore);
	void updateTable();
	HRESULT myCreateParamAccessor(ICommand*   pICmd, HACCESSOR*  phAccessor, IAccessor** ppIAccessor);
	

public:

	static Manager * GetManager();
	void printStudents(std::ostream & _stream);
	void removeStudent(int id);
	bool Manager::addStudent(int id, const char * firstName, const char * lastName, int age, int averageScore);
	
};

#endif 