#include "Student.h"


Student::Student(int _idStudent,
	const char * _firstName,
	const char * _lastName,
	int _age,
	int _score):
	m_idStudent(_idStudent)
{
	if (_age < 0)
		throw "Error with age";
	m_age = _age;
	if (_score < 0 || _score > 5)
		throw "Score must be >=0 and <=5";
	m_score = _score;
	m_firstName = new char[30];
	m_lastName = new char[30];
	strcpy(m_firstName, _firstName);
	strcpy(m_lastName, _lastName);
}

Student::Student()
{
	m_firstName = new char[30];
	m_lastName = new char[30];
	m_idStudent = 0;
	m_age = 0;
	m_score = 0;
}

Student::~Student()
{
	delete m_firstName;
	delete m_lastName;
}

void Student::printStudentInfo(std::ostream & _stream)
{
	_stream << m_idStudent << ' ' << m_firstName << ' ' << m_lastName << " Age:" << m_age << " Averege score:" << m_score << '\n';
}