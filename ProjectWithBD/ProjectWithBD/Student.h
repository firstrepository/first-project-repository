#ifndef STUDENT_H
#define STUDENT_H

#include "Manager.h"

class Student
{
public:

	int m_idStudent;
	char * m_firstName;
	char * m_lastName;
	int m_age;
	int m_score;

	Student(int _idStudent,
		const char * _firstName,
		const char * _lastName,
		int _age,
		int _score);

	Student();
	~Student();

	void printStudentInfo(std::ostream & _stream);

};

#endif