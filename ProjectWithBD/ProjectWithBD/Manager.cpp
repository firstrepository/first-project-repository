#include "Manager.h"
#include "Student.h"

Manager * Manager::ms_manager;

Manager * Manager::GetManager()
{
	if (!ms_manager)
	{
		ms_manager = new Manager();
		ms_manager->getRows();

		atexit(&Manager::DestroyManager);
	}
	return ms_manager;
}

void Manager::DestroyManager()
{
	delete ms_manager;
	ms_manager = nullptr;
}

Manager::Manager()
{
	if (FAILED(CoGetMalloc(MEMCTX_TASK, &pIMalloc)))
		throw "CoGetMalloc";

	pIRowset = NULL;
	pIMalloc = NULL;
	pColumnsInfo = NULL;

	// �������� �������
	pIDBInitialize = NULL;
	HRESULT hr = CoCreateInstance(CLSID_MSDASQL, NULL, CLSCTX_INPROC_SERVER, IID_IDBInitialize, (void **)&pIDBInitialize);

	// ���������� ���������� �������������
	pDBPROP = new DBPROP[4];
	initParamOfInit();

	// ��������� �������
	DBPROPSET mDBPROPSET;
	mDBPROPSET.guidPropertySet = DBPROPSET_DBINIT;
	mDBPROPSET.cProperties = 4;
	mDBPROPSET.rgProperties = pDBPROP;

	IDBProperties * pIDBProperties;
	pIDBInitialize->QueryInterface(IID_IDBProperties, (void**)&pIDBProperties);
	hr = pIDBProperties->SetProperties(1, &mDBPROPSET);
	pIDBProperties->Release();

	SysFreeString(pDBPROP[1].vValue.bstrVal);
	SysFreeString(pDBPROP[2].vValue.bstrVal);
	SysFreeString(pDBPROP[3].vValue.bstrVal);

	// ������������� �������
	hr = pIDBInitialize->Initialize();

	// �������� �����
	IDBCreateSession * pIDBCreateSession;
	pIDBInitialize->QueryInterface(IID_IDBCreateSession, (void**) &pIDBCreateSession);

	// ������� �������
	hr = pIDBCreateSession->CreateSession(NULL, IID_IDBCreateCommand, (IUnknown**)&pIDBCreateCommand);
	pIDBCreateSession->Release();	
}

Manager::~Manager()
{
	updateTable();
	delete pDBPROP;
	pIDBCreateCommand->Release();
	pIDBInitialize->Uninitialize();
	pIDBInitialize->Release();

	for (int i = 0; i < students.size(); i++)
		delete students[i];
}

void Manager::initParamOfInit()
{
	pDBPROP[0].dwPropertyID = DBPROP_INIT_PROMPT;
	pDBPROP[0].colid = DB_NULLID;
	pDBPROP[0].dwOptions = DBPROPOPTIONS_REQUIRED;
	VariantInit(&pDBPROP[0].vValue);
	pDBPROP[0].vValue.vt = VT_I2;
	pDBPROP[0].vValue.iVal = DBPROMPT_NOPROMPT;

	pDBPROP[1].dwPropertyID = DBPROP_INIT_DATASOURCE;
	pDBPROP[1].colid = DB_NULLID;
	pDBPROP[1].dwOptions = DBPROPOPTIONS_REQUIRED;
	VariantInit(&pDBPROP[1].vValue);
	pDBPROP[1].vValue.vt = VT_BSTR;
	pDBPROP[1].vValue.bstrVal = SysAllocString(OLESTR("MYDATABASE"));;  // �������� ��� ��������� ������ 

	pDBPROP[2].dwPropertyID = DBPROP_AUTH_USERID;
	VariantInit(&pDBPROP[2].vValue);
	pDBPROP[2].vValue.vt = VT_BSTR;
	pDBPROP[2].vValue.bstrVal = SysAllocString(OLESTR("SANYABD")); // ��� ������������
	pDBPROP[2].colid = DB_NULLID;
	pDBPROP[2].dwOptions = DBPROPOPTIONS_REQUIRED;

	pDBPROP[3].dwPropertyID = DBPROP_AUTH_PASSWORD;
	VariantInit(&pDBPROP[3].vValue);
	pDBPROP[3].vValue.vt = VT_BSTR;
	pDBPROP[3].vValue.bstrVal = SysAllocString(OLESTR("123")); // ������
	pDBPROP[3].colid = DB_NULLID;
	pDBPROP[3].dwOptions = DBPROPOPTIONS_REQUIRED;
}

bool Manager::insertRow(int id, const char * firstName, const char * lastName, int age, int averageScore)
{
		ICommandPrepare*    pICommandPrepare;
		ICommandWithParameters* pICmdWithParams;
		IAccessor*          pIAccessor;

		char* str = new char[255];
		sprintf(str, "insert into students (id, first_name, last_name, age, average_score) values(? , '%s' , '%s' , ? , ? ) ", firstName, lastName);

		WCHAR *wSQLString;
		int nChars = MultiByteToWideChar(CP_ACP, 0, str, -1, NULL, 0);
		wSQLString = new WCHAR[nChars];
		MultiByteToWideChar(CP_ACP, 0, str, -1, (LPWSTR)wSQLString, nChars);

		DBPARAMS            Params;
		long                cRowsAffected;
		HACCESSOR           hParamAccessor;
							newStudent = new Student();
		ULONG               nParams = 3;

		DBPARAMBINDINFO     rgParamBindInfo[] =
		{
			OLESTR(""),   OLESTR(""),	0,  DBPARAMFLAGS_ISINPUT,  0, 0,
			OLESTR(""),   OLESTR(""),	0,  DBPARAMFLAGS_ISINPUT,  0, 0,
			OLESTR(""),	  OLESTR(""),	0,  DBPARAMFLAGS_ISINPUT,  0, 0	};
	
		ULONG rgParamOrdinals[] = { 1, 2, 3 };

		ICommandText * pICommandText;
		HRESULT hr = pIDBCreateCommand->CreateCommand(NULL, IID_ICommandText, (IUnknown**)&pICommandText);
		pICommandText->SetCommandText(DBGUID_DBSQL, wSQLString);

		// Set parameter information.
		pICommandText->QueryInterface(IID_ICommandWithParameters, (void**)&pICmdWithParams);
		pICmdWithParams->SetParameterInfo(nParams, rgParamOrdinals, rgParamBindInfo);
		pICmdWithParams->Release();

		// Prepare the command.
		pICommandText->QueryInterface(IID_ICommandPrepare, (void**)&pICommandPrepare);
		if (FAILED(pICommandPrepare->Prepare(0)))
		{
			pICommandPrepare->Release();
			pICommandText->Release();
			return false;
		}
		pICommandPrepare->Release();

		// Create parameter accessors.
		if (FAILED(myCreateParamAccessor(pICommandText, &hParamAccessor, &pIAccessor)))
		{
			pICommandText->Release();
			return false;
		}

		Params.pData = newStudent;      // pData is the buffer pointer
		Params.cParamSets = 1;            // Number of sets of parameters
		Params.hAccessor = hParamAccessor;// Accessor to the parameters

		// Specify the parameter information.
		newStudent->m_idStudent = id;
		newStudent->m_age		= age;
		newStudent->m_score		= averageScore;

		// Execute the command.
		pICommandText->Execute(NULL, IID_NULL, &Params, &cRowsAffected, NULL);

		pIAccessor->ReleaseAccessor(hParamAccessor, NULL);
		pIAccessor->Release();
		pICommandText->Release();

		return true;
}

void Manager::clearTable()
{
	ICommandText * pICommandText;
	HRESULT hr = pIDBCreateCommand->CreateCommand(NULL, IID_ICommandText, (IUnknown**)&pICommandText);

	LPCOLESTR sqlRequest = OLESTR("delete from students;");
	pICommandText->SetCommandText(DBGUID_DBSQL, sqlRequest);

	LONG rowsCounter;
	hr = pICommandText->Execute(NULL, IID_NULL, NULL, &rowsCounter, (IUnknown**)&pIRowset);
	pICommandText->Release();
}

void Manager::updateTable()
{
	clearTable();

	for (int i = 0; i < students.size(); i++)
		insertRow(students[i]->m_idStudent,
		students[i]->m_firstName,
		students[i]->m_lastName,
		students[i]->m_age,
		students[i]->m_score);
}

void Manager::getRows()
{
	ICommandText * pICommandText;
	HRESULT hr = pIDBCreateCommand->CreateCommand(NULL, IID_ICommandText, (IUnknown**)&pICommandText);
	

	LPCOLESTR sqlRequest = OLESTR("select * from students");
	pICommandText->SetCommandText(DBGUID_DBSQL, sqlRequest);

	LONG rowsCounter;
	hr = pICommandText->Execute(NULL, IID_IRowset, NULL, &rowsCounter, (IUnknown**)&pIRowset);
	pICommandText->Release();

	//��������� �������� ������ �������
	IColumnsInfo * pIColumnsInfo;
	pIRowset->QueryInterface(IID_IColumnsInfo, (void**)&pIColumnsInfo);

	DBCOLUMNINFO* pColInfo = NULL;
	ULONG         nColsCount;
	OLECHAR*      pColStringsBuffer = NULL;

	hr = pIColumnsInfo->GetColumnInfo(&nColsCount, &pColInfo, &pColStringsBuffer);
	pColumnsInfo = pColInfo;
	pIColumnsInfo->Release();

	//���������� ���������� ��� �������� ������
	DBBINDING *  pDBBind = NULL;
	pDBBind = new DBBINDING[nColsCount];

	ULONG  cbRow = 0;

	memset(pDBBind, 0, sizeof(DBBINDING)* nColsCount);

	for (ULONG nCurrentCol = 0; nCurrentCol < nColsCount; nCurrentCol++)
	{
		pDBBind[nCurrentCol].iOrdinal = nCurrentCol + 1;
		pDBBind[nCurrentCol].obValue = cbRow;
		pDBBind[nCurrentCol].dwPart = DBPART_VALUE;
		pDBBind[nCurrentCol].dwMemOwner = DBMEMOWNER_CLIENTOWNED;
		pDBBind[nCurrentCol].eParamIO = DBPARAMIO_NOTPARAM;
		pDBBind[nCurrentCol].bPrecision = pColInfo[nCurrentCol].bPrecision;
		pDBBind[nCurrentCol].bScale = pColInfo[nCurrentCol].bScale;
		pDBBind[nCurrentCol].wType = pColInfo[nCurrentCol].wType;

			pDBBind[nCurrentCol].cbMaxLen =
			pColInfo[nCurrentCol].ulColumnSize;
		cbRow += pDBBind[nCurrentCol].cbMaxLen;
	}


	//���������� �������� ������
	IAccessor * pIAccessor;
	pIRowset->QueryInterface(IID_IAccessor, (void**)&pIAccessor);

	DBBINDSTATUS* pDBBindStatus = NULL;
	pDBBindStatus = new DBBINDSTATUS[nColsCount];

	HACCESSOR hAccessor;
	pIAccessor->CreateAccessor(DBACCESSOR_ROWDATA, nColsCount,
		pDBBind, 0, &hAccessor, pDBBindStatus);

	//��������� ������ �������

	HROW rghRows[30];
	HROW* pRows = &rghRows[0];
	ULONG cObtainedRows;
	while (TRUE)
	{
		pIRowset->GetNextRows(0, 0, 30, &cObtainedRows, &pRows);
		if (cObtainedRows == 0)
			break;

		char* pRowValues;
		pRowValues = new char[cbRow];
		ULONG iRowCounter;

		for (iRowCounter = 0; iRowCounter < cObtainedRows; iRowCounter++)
		{
			pIRowset->GetData(rghRows[iRowCounter], hAccessor, pRowValues);

			int id, age, score;
			std::string firstName, lastName;

			ULONG nCurrentCol;
			for (nCurrentCol = 0; nCurrentCol < nColsCount; nCurrentCol++)
			{
				// pColInfo[nCurrentCol].wType
				// pRowValues[pDBBind[nCurrentCol].obValue]
				switch (nCurrentCol)
				{
				case 0:
					id = (int)pRowValues[pDBBind[nCurrentCol].obValue];
					break;
				case 1:
				{
						  char * data = &pRowValues[pDBBind[nCurrentCol].obValue];
						  firstName.push_back(data[0]);
						  for (int i = 1; i < strlen(data); i++)
						  if (data[i] != ' ' &&  data[i - 1] != ' ')
							  firstName.push_back(data[i]);
				}
					break;
				case 2:
				{
						  char * data = &pRowValues[pDBBind[nCurrentCol].obValue];
						  lastName.push_back(data[0]);
						  for (int i = 1; i < strlen(data); i++)
						  if (data[i] != ' ' &&  data[i - 1] != ' ')
							  lastName.push_back(data[i]);
				}
					break;
				case 3:
					age = (int)pRowValues[pDBBind[nCurrentCol].obValue];
					break;
				case 4:
					score = (int)pRowValues[pDBBind[nCurrentCol].obValue];
					break;
				}
			}
			students.push_back(new Student(id, firstName.c_str(), lastName.c_str(), age, score));
			firstName.clear();
			lastName.clear();
		}
	}
			pIRowset->ReleaseRows(cObtainedRows, rghRows, NULL, NULL, NULL);
			pIAccessor->ReleaseAccessor(hAccessor, NULL);
			pIAccessor->Release();
}

void Manager::printStudents(std::ostream & _stream)
{
	for (int i = 0; i < students.size(); i++)
		students[i]->printStudentInfo(_stream);
}

bool Manager::addStudent(int id, const char * firstName, const char * lastName, int age, int averageScore)
{
	for (int i = 0; i < students.size();i++)
	if (students[i]->m_idStudent == id)
		return false;

	students.push_back(new Student(id, firstName, lastName, age, averageScore));
	return true;
}

void Manager::removeStudent(int id)
{
	for (int i = 0; i < students.size(); i++)
	if (students[i]->m_idStudent == id)
	{
		students.erase(students.begin() + i);
		return;
	}
}

HRESULT Manager::myCreateParamAccessor( ICommand*   pICmd, HACCESSOR*  phAccessor, IAccessor** ppIAccessor )
{
	IAccessor*      pIAccessor;
	HACCESSOR       hAccessor;
	const ULONG     nParams = 3;
	DBBINDING       Bindings[nParams];
	DBBINDSTATUS    rgStatus[nParams]; 
	HRESULT         hr;

	for (ULONG i = 0; i < nParams; i++)
	{
		Bindings[i].iOrdinal = i + 1;
		Bindings[i].obLength = 0;
		Bindings[i].obStatus = 0;
		Bindings[i].pTypeInfo = NULL;
		Bindings[i].pObject = NULL;
		Bindings[i].pBindExt = NULL;
		Bindings[i].dwPart = DBPART_VALUE;
		Bindings[i].dwMemOwner = DBMEMOWNER_CLIENTOWNED;
		Bindings[i].eParamIO = DBPARAMIO_INPUT;
		Bindings[i].cbMaxLen = 0;
		Bindings[i].dwFlags = 0;
		Bindings[i].wType = DBTYPE_I4;
		Bindings[i].bPrecision = 0;
		Bindings[i].bScale = 0;
	}

	Bindings[0].obValue = offsetof(Student, m_idStudent);
	Bindings[1].obValue = offsetof(Student, m_age);
	Bindings[2].obValue = offsetof(Student, m_score);
	

	pICmd->QueryInterface(IID_IAccessor, (void**)&pIAccessor);

	hr = pIAccessor->CreateAccessor( DBACCESSOR_PARAMETERDATA, nParams, Bindings, sizeof(Student), &hAccessor, rgStatus );

	if (FAILED(hr))
		throw "Parameter accessor creation failed";
	else
	{
		*ppIAccessor = pIAccessor;
		*phAccessor = hAccessor;
	}
	return (hr);
}
